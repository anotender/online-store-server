const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

//body-parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

//mongoose
mongoose.connect('mongodb://admin:admin123@ds135974.mlab.com:35974/online-store-db', {useNewUrlParser: true})
    .then(() => {
        console.log('Successfully connected to db');
    }, failure => {
        console.error('Failed to connect to db');
        console.error(failure);
    });

let Product = mongoose.model('Product', {
    name: String,
    numberOfProducts: Number,
    price: Number,
    description: String,
    imageUrl: String,
    categories: [String]
});

let Order = mongoose.model('Order', {
    recipient: String,
    address: String,
    completionDate: Number,
    totalValue: Number,
    items: [{
        productId: String,
        numberOfOccurrences: Number,
        collected: Boolean
    }]
});

var port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log('listening on ' + port);
});

app.get('/products', (req, res) => {
    Product.find(function (err, products) {
        if (err) {
            console.error(err);
        } else if (products) {
            res.json(products);
        }
    });
});

app.get('/products/:id', (req, res) => {
    Product.findOne({_id: req.params['id']}, function (err, product) {
        if (err) {
            console.error(err);
        } else if (product) {
            res.json(product);
        }
    });
});

app.post('/products', (req, res) => {
    Product.create(req.body, function (err, product) {
        if (err) {
            console.error(err);
            res.status(400);
            res.json(err);
        } else if (product) {
            console.log('New product with ID ' + product._id + ' created');
            res.status(201);
            res.json(product);
        }
    });
});

app.put('/products/:id', (req, res) => {
    Product.findOneAndUpdate({_id: req.params['id']}, req.body, function (err, product) {
        if (err) {
            console.error(err);
            res.status(404);
            res.json(err);
        } else if (product) {
            console.log('Product with ID ' + req.params['id'] + ' updated');
            res.status(200);
            res.json(product);
        }
    });
});

app.delete('/products/:id', (req, res) => {
    Order.find(function (err, orders) {
        if (err) {
            console.error(err);
            res.status(404);
        } else if (orders) {
            let isProductInSomeOrder = orders
                .filter(order => order.completionDate === null)
                .some(order => order.items.some(item => item.productId === req.params['id']));
            if (isProductInSomeOrder) {
                res.status(400);
                res.json('Cannot delete product because it is in some order');
                return;
            }

            Product.findOneAndDelete({_id: req.params['id']}, function (err, product) {
                if (err) {
                    console.error(err);
                    res.status(404);
                    res.json(err);
                } else if (product) {
                    console.log('Product with ID ' + req.params['id'] + ' deleted');
                    res.status(200);
                    res.json(product);
                }
            });
        }
    });
});

app.get('/orders', (req, res) => {
    Order.find(function (err, orders) {
        if (err) {
            console.error(err);
            res.status(400);
        } else if (orders) {
            res.status(200);
            res.json(orders);
        }
    });
});

app.post('/orders', (req, res) => {
    let order = req.body;
    let productIds = req.body.items.map(item => item.productId);
    Product.find({_id: {$in: productIds}}, function (err, products) {
        if (err) {
            console.error(err);
            res.status(400);
            res.json(err);
        } else if (products && products.length === productIds.length) {
            let productWithNotEnoughItems = products.find(product => {
                const item = order.items.find(item => item.productId === product._id.toString());
                return product.numberOfProducts < item.numberOfOccurrences;
            });

            if (productWithNotEnoughItems) {
                console.error('Not enough items of ' + productWithNotEnoughItems._id + ' product in order');
                res.status(400);
                res.json('Not enough items of ' + productWithNotEnoughItems._id + ' product in order');
                return;
            }

            products.forEach(product => {
                const item = order.items.find(item => item.productId === product._id.toString());
                product.numberOfProducts -= item.numberOfOccurrences;
                Product.findOneAndUpdate({_id: product._id}, product, function (err) {
                    if (err) {
                        console.error(err);
                    }
                });
            });
            Order.create(req.body, function (err, order) {
                if (err) {
                    console.error(err);
                    res.status(400);
                    res.json(err);
                } else if (order) {
                    console.log('New order with ID ' + order._id + ' created');
                    res.status(201);
                    res.json(order);
                }
            });
        } else {
            console.error('Product missing');
            res.status(400);
            res.json('Product missing');
        }
    });
});

app.put('/orders/:id', (req, res) => {
    Order.findOneAndUpdate({_id: req.params['id']}, req.body, function (err, order) {
        if (err) {
            console.error(err);
            res.status(404);
            res.json(err);
        } else if (order) {
            console.log('Order with ID ' + req.params['id'] + ' updated');
            res.status(200);
            res.json(order);
        }
    });
});